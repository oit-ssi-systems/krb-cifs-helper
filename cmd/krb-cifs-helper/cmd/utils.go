package cmd

import (
	"fmt"
	"os"
	"path"
	"strings"

	"gopkg.in/yaml.v3"
)

type shareList struct {
	Shares []share `json:"shares"`
}

type share struct {
	MountOptions string `yaml:"mount_options"`
	Server       string `yaml:"server"`
	ShareName    string `yaml:"share_name"`
	FriendlyName string `yaml:"friendly_name"`
}

func loadShares(f string) ([]share, error) {
	var storage shareList
	yamlFile, err := os.ReadFile(path.Clean(f))
	if err != nil {
		return nil, err
	}
	if err := yaml.Unmarshal(yamlFile, &storage); err != nil {
		return nil, err
	}

	return storage.Shares, nil
}

func extractShare(a string) (string, error) {
	cmdData := strings.SplitN(a, "-", 2)
	if len(cmdData) != 2 {
		return "", fmt.Errorf("could not extract share from %v", a)
	}
	return cmdData[1], nil
}

func getMount(shares []share, target string) (string, error) {
	for _, share := range shares {
		if share.FriendlyName == target {
			return fmt.Sprintf("%v ://%v/%v", share.MountOptions, share.Server, share.ShareName), nil
		}
	}
	return "", fmt.Errorf("could not find share information on: %v", target)
}
