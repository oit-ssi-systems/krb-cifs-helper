/*
Package main is the main executable
*/
package main

import (
	"log/slog"
	"os"

	"gitlab.oit.duke.edu/oit-ssi-systems/krb-cifs-helper/cmd/krb-cifs-helper/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		slog.Error("fatal error", "error", err)
		os.Exit(2)
	}
}
