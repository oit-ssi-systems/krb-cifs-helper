/*
Package cmd holds all the actual command data
*/
package cmd

import (
	"cmp"
	"fmt"
	"io"
	"log/slog"
	"log/syslog"
	"os"
	"time"

	"github.com/charmbracelet/log"
)

// Execute executes the CLI
func Execute() error {
	initLogging()
	if len(os.Args) != 2 {
		return fmt.Errorf("usage: %v netid-share", os.Args[0])
	}

	targetName, err := extractShare(os.Args[1])
	if err != nil {
		return err
	}

	shares, err := loadShares(cmp.Or(os.Getenv("KRB_CIFS_STORAGE_PATH"), "/etc/storage.yaml"))
	if err != nil {
		return err
	}

	mountString, err := getMount(shares, targetName)
	if err != nil {
		return err
	}
	fmt.Println(mountString)
	return nil
}

func initLogging() {
	logwriter, err := syslog.New(syslog.LOG_NOTICE, "krb-cifs-helper")
	if err != nil {
		panic(err)
	}
	lopts := log.Options{
		TimeFormat: time.StampMilli,
		Prefix:     "krb-cifs-helper 🔐 ",
	}
	slog.SetDefault(slog.New(log.NewWithOptions(
		io.MultiWriter(logwriter, os.Stderr),
		lopts,
	)))
}
