package cmd

import (
	"testing"
)

func TestExtractTargetShare(t *testing.T) {
	tests := []struct {
		Argument  string
		Want      string
		ShouldErr bool
	}{
		{
			Argument: "foo-bar",
			Want:     "bar",
		},
		{
			Argument:  "foo",
			ShouldErr: true,
		},
	}

	for _, test := range tests {
		got, err := extractShare(test.Argument)
		if test.ShouldErr {
			if err == nil {
				t.Errorf("Expected error to happen, but it didn't")
			}
		}
		if got != test.Want {
			t.Errorf("Wanted %v but got %v", test.Want, got)
		}
	}
}

func TestLoadShares(t *testing.T) {
	got, err := loadShares("./testdata/storage.yaml")
	if err != nil {
		t.Fatalf("got unexpected error: %v", err)
	}
	if len(got) != 2 {
		t.Errorf("got unexpected length: %v\nbut expected: %v\n", len(got), 2)
	}

	_, err = loadShares("never-exists.yaml")
	if err == nil {
		t.Errorf("expected an error but got none")
	}
}

func TestGetMounts(t *testing.T) {
	got, err := getMount([]share{
		{FriendlyName: "bar", Server: "storage.example.edu", MountOptions: "-fstype=cifs", ShareName: "my-share"},
		{FriendlyName: "foo", Server: "storage.example.edu", MountOptions: "-fstype=cifs", ShareName: "my-other-share"},
	}, "foo")
	if err != nil {
		t.Fatalf("got unexpected error: %v", err)
	}
	expected := "-fstype=cifs ://storage.example.edu/my-other-share"
	if got != expected {
		t.Errorf("expected: %v\n but got: %v\n", expected, got)
	}

	_, err = getMount([]share{
		{FriendlyName: "bar", Server: "storage.example.edu", MountOptions: "-fstype=cifs", ShareName: "my-share"},
		{FriendlyName: "foo", Server: "storage.example.edu", MountOptions: "-fstype=cifs", ShareName: "my-other-share"},
	}, "baz")
	expectErr := "could not find share information on: baz"
	if err.Error() != expectErr {
		t.Errorf("expected error: %v\n but got: %v\n", expectErr, err.Error())
	}
}
